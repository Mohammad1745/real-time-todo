<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoRequest;
use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function todo()
    {
        $todos = Auth::user()->todos;
        return view('todo', compact('todos'));
    }
    public function index()
    {
        $todos = Auth::user()->todos;
        return response()->json([
            'success' => true,
            'todos' => $todos,
        ]);
    }
    public function store(TodoRequest $request)
    {
        $todo = Todo::create([
            'user_id' => Auth::user()->id,
            'title' => $request->title,
        ]);
        return response()->json([
            'success' => true,
            'todo' => $todo,
        ]);
    }
    public function destroy(Request $request)
    {
        $todo = Todo::find($request->id)->delete();
        return response()->json([
            'success' => true,
        ]);
    }
}

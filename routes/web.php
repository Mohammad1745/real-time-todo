<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/todo', 'TodoController@todo')->name('todo'); // load the page
Route::get('/todo/index', 'TodoController@index')->name('todo.index'); // json response with todo list
Route::post('/todo/store', 'TodoController@store')->name('todo.store'); // save the new todo
Route::post('/todo/destroy', 'TodoController@destroy')->name('todo.destroy'); // delete todo

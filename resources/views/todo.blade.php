@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Todos</div>

                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" id="title" class="form-control">
                                </div>
                                <button id="submit"  class="btn btn-outline-primary">Add</button>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <ol id="todos">


                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $('document').ready(function () {
        refreshList();

        $('#submit').click(function () {
            let title = $('#title').val();
            saveTodo(title);
        });

        $('#todos').on('click', '.delete-todo', function () {
            let id = $(this).data('id');
            deleteTodo(id);
        });

        function saveTodo(title) {
            $.ajax({
                url: '{{route('todo.store')}}',
                method: 'POST',
                data: {
                    '_token': '{{csrf_token()}}',
                    'title': title,
                }
            }).done(function (data) {
                refreshList();
            }).fail(function (error) {
                console.log(error);
            });
        }

        function deleteTodo(id) {
            $.ajax({
                url: '{{route('todo.destroy')}}',
                method: 'POST',
                data: {
                    '_token': '{{csrf_token()}}',
                    'id': id,
                }
            }).done(function (data) {
                refreshList();
            }).fail(function (error) {
                console.log(error);
            });
        }

        function refreshList() {
            $.ajax({
                url: '{{route('todo.index')}}',
                method: 'GET',
            }).done(function (data) {
                injectDynamicDom(data.todos);
                resetInputFields();
            }).fail(function (error) {
                console.log(error);
            });
        }

        function injectDynamicDom(todos) {
            let html = '';

            for (let i=0; i<todos.length; i++){
                html += '<li class="mb-1 mt-1">'+
                        '<div class="row">'+
                            '<div class="col-md-10">'+
                                todos[i].title+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<button data-id="'+todos[i].id+'" class="btn btn-outline-danger delete-todo">' +
                                    '<svg class="bi bi-archive-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">\n' +
    '                                    <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM6 7a.5.5 0 000 1h4a.5.5 0 000-1H6zM.8 1a.8.8 0 00-.8.8V3a.8.8 0 00.8.8h14.4A.8.8 0 0016 3V1.8a.8.8 0 00-.8-.8H.8z" clip-rule="evenodd"/>\n' +
    '                                </svg>' +
                                '</button>'+
                            '</div>'+
                        '</div>'+
                    '</li>'+
                    '<div style="width: 90%; background-color: #1d2124; padding: 1px;"></div>';
            }
            $('#todos').html(html);
        }

        function resetInputFields() {
            $("#title").val('');
        }
    });
</script>
@endsection
